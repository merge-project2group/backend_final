import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  UseGuards,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { CheckworksService } from './checkworks.service';
import { CreateCheckworkDto } from './dto/create-checkwork.dto';
import { UpdateCheckworkDto } from './dto/update-checkwork.dto';
import { AuthGuard } from 'src/auth/auth.guard';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { uuid } from 'uuidv4';
import { extname } from 'path';

//@UseGuards(AuthGuard)
@Controller('checkworks')
export class CheckworksController {
  constructor(private readonly checkworksService: CheckworksService) {}

  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/checkworks',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  create(
    @Body() createCheckworkDto: CreateCheckworkDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    return this.checkworksService.create(createCheckworkDto);
  }

  @Get()
  findAll() {
    return this.checkworksService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.checkworksService.findOne(+id);
  }

  @Get('indbybranchid/:id')
  findbybranchid(@Param('id') id: number) {
    return this.checkworksService.findbybranchid(id);
  }

  @Post(':id')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/checkworks',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  update(
    @Param('id') id: string,
    @Body() updateCheckworkDto: UpdateCheckworkDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      updateCheckworkDto.image = file.filename;
    }
    return this.checkworksService.update(+id, updateCheckworkDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkworksService.remove(+id);
  }
  @Post('upload')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/checkworks',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  uploadFile(
    @Body() checkwork: { name: string; age: number },
    @UploadedFile() file: Express.Multer.File,
  ) {
    console.log(checkwork);
    console.log(file.filename);
    console.log(file.path);
  }
}
