import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCheckworkDto } from './dto/create-checkwork.dto';
import { UpdateCheckworkDto } from './dto/update-checkwork.dto';
import { Repository } from 'typeorm';
import { Checkwork } from './entities/checkwork.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class CheckworksService {
  constructor(
    @InjectRepository(Checkwork)
    private checkworksRepository: Repository<Checkwork>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}
  async create(createCheckworkDto: CreateCheckworkDto) {
    const checkwork = new Checkwork();
    checkwork.fullname = createCheckworkDto.fullname;
    checkwork.date = createCheckworkDto.date;
    checkwork.timein = createCheckworkDto.timein;
    checkwork.timeout = createCheckworkDto.timeout;
    // eslint-disable-next-line prettier/prettier
    const user = await this.usersRepository.findOne({
      where: { email: createCheckworkDto.fullname },
      relations: ['branch'],
    });
    if (!user) {
      throw new NotFoundException();
    }
    checkwork.branch = user.branch;
    checkwork.User = user;
    checkwork.status = createCheckworkDto.status;
    return this.checkworksRepository.save(checkwork);
  }

  findAll() {
    return this.checkworksRepository.find({});
  }

  findbybranchid(id: number) {
    console.log(id);

    return this.checkworksRepository.find({
      where: { branch: { id: id } },
    });
  }

  findOne(id: number) {
    return this.checkworksRepository.findOne({
      where: { id },
    });
  }

  async update(id: number, updateCheckworkDto: UpdateCheckworkDto) {
    const checkworkToUpdate = await this.checkworksRepository.findOneOrFail({
      where: { id },
    });
    checkworkToUpdate.fullname = updateCheckworkDto.fullname;
    checkworkToUpdate.date = updateCheckworkDto.date;
    checkworkToUpdate.timein = updateCheckworkDto.timein;
    checkworkToUpdate.timeout = updateCheckworkDto.timeout;
    checkworkToUpdate.status = updateCheckworkDto.status;
    return this.checkworksRepository.save(checkworkToUpdate);
  }

  async remove(id: number) {
    const deleteProduct = await this.checkworksRepository.findOneOrFail({
      where: { id },
    });
    await this.checkworksRepository.remove(deleteProduct);
    return deleteProduct;
  }
}
