import { Test, TestingModule } from '@nestjs/testing';
import { CheckworksService } from './checkworks.service';

describe('CheckworksService', () => {
  let service: CheckworksService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CheckworksService],
    }).compile();

    service = module.get<CheckworksService>(CheckworksService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
