import { Branch } from 'src/Branch/entities/Branch.entity';
import { User } from 'src/users/entities/user.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';

@Entity()
export class Checkwork {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  fullname: string;

  @Column()
  date: string;

  @Column()
  timein: string;

  @Column()
  timeout: string;

  @Column()
  status: string;

  @ManyToOne(() => User, (users) => users.checkwork, { cascade: true })
  User: User;

  @ManyToOne(() => Branch, (Branchs) => Branchs.checkwork, { cascade: true })
  branch: Branch;
}
