import { Injectable } from '@nestjs/common';
import { DataSource } from 'typeorm';

@Injectable()
export class ReportService {
  constructor(private dataSource: DataSource) {}
  reportAllBillMonth() {
    //ALL
    return this.dataSource.query(`SELECT SUM(price) AS TotalPrice
      FROM bill
      WHERE status = 'Paid';`);
  }
  reportExpenses() {
    return this.dataSource
      .query(`SELECT SUM(total_expenses) AS total_combined_expenses
      FROM (
          SELECT SUM(salary) AS total_expenses
          FROM salary
          UNION ALL
          SELECT SUM(price) AS total_expenses
          FROM bill
      ) AS combined_expenses;`);
  }

  reportBillMonth() {
    //All Month
    return this.dataSource
      .query(`SELECT DATE_FORMAT(timepay, '%Y-%m') AS month, SUM(price) AS TotalPrice 
      FROM bill 
      WHERE status = 'Paid' 
      GROUP BY DATE_FORMAT(timepay, '%Y-%m');`);
  }

  reportBillYear() {
    //All Year
    return this.dataSource
      .query(`SELECT DATE_FORMAT(timepay, '%Y') AS year, SUM(price) AS TotalPrice
      FROM bill
      WHERE status = 'Paid'
      GROUP BY DATE_FORMAT(timepay, '%Y');`);
  }

  reportBillYear2023() {
    //Year 2023
    return this.dataSource
      .query(`SELECT SUM(price) AS total_price, DATE_FORMAT(timepay, '%m/%Y') AS formatted_timepay
      FROM bill
      WHERE YEAR(timepay) = 2023
      GROUP BY DATE_FORMAT(timepay, '%m/%Y');`);
  }

  reportBillYear2024() {
    //Year 2024
    return this.dataSource
      .query(`SELECT SUM(price) AS total_price, DATE_FORMAT(timepay, '%m/%Y') AS formatted_timepay
      FROM bill
      WHERE YEAR(timepay) = 2024
      GROUP BY DATE_FORMAT(timepay, '%m/%Y');`);
  }

  reportIncomebyMonth() {
    return this.dataSource
      .query(`SELECT DATE_FORMAT(created, '%Y-%m') AS Month, SUM(total) AS Income
      FROM \`order\`
      GROUP BY DATE_FORMAT(created, '%Y-%m');`);
  }

  reportIncomebyYear() {
    return this.dataSource
      .query(`SELECT DATE_FORMAT(created, '%Y') AS Year, SUM(total) AS Income
      FROM \`order\`
      WHERE DATE_FORMAT(created, '%Y') = YEAR(CURRENT_DATE()) 
      GROUP BY DATE_FORMAT(created, '%Y'); `);
  }
  reportCombind() {
    return this.dataSource.query(`
    SELECT Salary, Expenditure, Income, Year
    FROM (
        SELECT
            (SELECT SUM(salary) FROM salary WHERE status='Paid') AS Salary,
            (SELECT SUM(price) FROM bill WHERE status='Paid') AS Expenditure,
            (SELECT SUM(total) FROM \`order\`) AS Income,
            YEAR(CURRENT_DATE()) AS Year 
    ) AS CombinedData
    WHERE Year = '2024'; 
    `);
  }
  // Bill All B 1
  reportBillBrach1() {
    return this.dataSource.query(`SELECT SUM(total) AS total_sum, DATE(created) AS created_date
    FROM \`order\`
    GROUP BY DATE(created);`);
  }
  // Bill All B 2
  reportBillBrach2() {
    return this.dataSource.query(`SELECT SUM(total) AS total_sum, DATE_FORMAT(created, '%Y-%m') AS created_month
    FROM \`order\`
    GROUP BY DATE_FORMAT(created, '%Y-%m');`);
  }
  // Bill All B 3
  reportBillBrach3() {
    return this.dataSource.query(`SELECT SUM(total) AS total_sum, YEAR(created) AS created_year
    FROM \`order\`
    GROUP BY YEAR(created);`);
  }
  reportPieAll() {
    return this.dataSource.query(`SELECT (Income.total_amount + SalaryExpenses.total_salary + BillExpenses.total_price) AS expenses , Income.total_amount AS income_data
    FROM
        (SELECT SUM(total) AS total_amount FROM \`order\` WHERE SUBSTRING(created, 1, 4) = '2024' GROUP BY SUBSTRING(created, 1, 4)) AS Income,
        (SELECT SUM(salary) AS total_salary FROM salary WHERE SUBSTRING(\`date\`, 1, 4) = '2024') AS SalaryExpenses,
        (SELECT SUM(price) AS total_price FROM bill WHERE EXTRACT(YEAR FROM \`time\`) = '2024') AS BillExpenses;`);
  }
  reportStock() {
    return this.dataSource.query(`SELECT SUM(used) AS total_used, DATE_FORMAT(Savedate, '%Y-%m') AS month_year
    FROM ingredient
    GROUP BY DATE_FORMAT(Savedate, '%Y-%m');`);
  }
}
