import { Controller, Get, Param } from '@nestjs/common';
import { ReportService } from './report.service';

@Controller('report')
export class ReportController {
  constructor(private reportService: ReportService) {}
  @Get('/reportAllBillMonth')
  reportAllBillMonth() {
    return this.reportService.reportAllBillMonth();
  }
  @Get('/reportExpenses')
  reportExpenses() {
    return this.reportService.reportExpenses();
  }
  @Get('/reportBillYear') //Bill All Year
  reportฺBillYear() {
    return this.reportService.reportBillYear();
  }
  @Get('/reportBillMonth') //Bill Month
  reportBillMonth() {
    return this.reportService.reportBillMonth();
  }

  @Get('reportBillYear2023')
  reportBillYear2023() {
    return this.reportService.reportBillYear2023();
  }

  @Get('reportBillYear2024')
  reportBillYear2024() {
    return this.reportService.reportBillYear2024();
  }

  @Get('/reportIncomebyMonth')
  reportIncomebyMonth() {
    return this.reportService.reportIncomebyMonth();
  }

  @Get('reportIncomebyYear')
  reportIncomebyYear() {
    return this.reportService.reportIncomebyYear();
  }

  @Get('reportCombind')
  reportCombind() {
    return this.reportService.reportCombind();
  }
  @Get('reportBillBrach1')
  reportBillBrach1() {
    return this.reportService.reportBillBrach1();
  }
  @Get('reportBillBrach2')
  reportBillBrach2() {
    return this.reportService.reportBillBrach2();
  }
  @Get('reportBillBrach3')
  reportBillBrach3() {
    return this.reportService.reportBillBrach3();
  }
  @Get('reportPieAll')
  reportPieAll() {
    return this.reportService.reportPieAll();
  }
  @Get('reportStock')
  reportStock() {
    return this.reportService.reportStock();
  }
}
