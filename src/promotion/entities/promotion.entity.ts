import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export class Promotion {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  discount: number;

  @Column()
  type: string;

  @Column({ default: '0' })
  startdate: string;

  @Column({ default: '0' })
  enddate: string;

  @Column({ default: '0' })
  status: string;
}
