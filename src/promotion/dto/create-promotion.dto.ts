import { IsNotEmpty, Length } from 'class-validator';
export class CreatePromotionDto {
  // @IsNotEmpty()
  // @Length(8, 32)
  //name: string;
  //@MinLength(1)
  //type: ('bath' | '%')[];
  //name: string;
  //discount: number;
  //type: ('bath' | '%')[];

  name: string;
  discount: number;
  type: string;
}
