import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Bill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  invoice: string;

  @Column()
  namepd: string;

  @Column()
  amountbill: number;

  @Column()
  units: string;

  @Column()
  priceperunits: number;

  @Column()
  discount: number;

  @Column()
  price: number;

  @Column()
  time: string;

  @Column()
  timepay: string;

  @Column()
  status: string;

  @Column()
  isChecked?: boolean;
}
