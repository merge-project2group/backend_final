import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RolesModule } from './roles/roles.module';
import { TypesModule } from './types/types.module';
import { ProductsModule } from './products/products.module';
import { UsersModule } from './users/users.module';
import { OrdersModule } from './orders/orders.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './users/entities/user.entity';
import { Role } from './roles/entities/role.entity';
import { Type } from './types/entities/type.entity';
import { Product } from './products/entities/product.entity';
import { Order } from './orders/entities/order.entity';
import { OrderItem } from './orders/entities/orderItem.entity';
import { DataSource } from 'typeorm';
import { AuthModule } from './auth/auth.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { Bill } from './BILLSUCCESS/entities/bill.entity';
import { Branch } from './Branch/entities/Branch.entity';
import { Promotion } from './promotion/entities/promotion.entity';
import { BillModule } from './BILLSUCCESS/bills.module';
import { BranchModule } from './Branch/branchs.module';
import { PromotionModule } from './promotion/promotion.module';
import { IngredientsModule } from './ingredients/ingredients.module';
import { Ingredient } from './ingredients/entities/ingredient.entity';
import { Salary } from './Salary/entities/salary.entity';
import { SalaryModule } from './Salary/salarys.module';
import { Member } from './member/entities/member.entity';
import { MembersModule } from './member/members.module';
import { ReportModule } from './report/report.module';
import { Checkwork } from './checkwork/entities/checkwork.entity';
import { CheckworksModule } from './checkwork/checkworks.module';
import { OrderIngredient } from './Order Ingredients/entities/OrderIngredient.entity';
import { OrderIngredientsModule } from './Order Ingredients/orderingredients.module';

@Module({
  imports: [
    // TypeOrmModule.forRoot({
    //   type: 'sqlite',
    //   database: 'database.sqlite',
    //   synchronize: true,
    //   logging: false,
    // entities: [
    //   User,
    //   Role,
    //   Type,
    //   Product,
    //   Order,
    //   OrderItem,
    //   Bill,
    //   Branch,
    //   Promotion,
    //   Ingredient,
    //   Salary,
    //   Member,
    //   Checkwork,
    // ],
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'angsila.informatics.buu.ac.th',
      port: 3306,
      username: 'cscamp07',
      password: 'A3djxsFuJl',
      database: 'cscamp07',
      entities: [
        User,
        Role,
        Type,
        Product,
        Order,
        OrderItem,
        Bill,
        Branch,
        Promotion,
        Ingredient,
        Salary,
        Member,
        Checkwork,
        OrderIngredient,
      ],
      synchronize: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    OrderIngredientsModule,
    RolesModule,
    TypesModule,
    ProductsModule,
    UsersModule,
    OrdersModule,
    AuthModule,
    BillModule,
    BranchModule,
    PromotionModule,
    IngredientsModule,
    SalaryModule,
    CheckworksModule,
    MembersModule,
    ReportModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
