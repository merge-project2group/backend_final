import { IsNotEmpty, Length } from 'class-validator';
export class CreateBranchDto {
  name: string;

  // @IsNotEmpty()
  // @Length(8, 32)
  tel: string;

  // @IsNotEmpty()
  // @Length(8, 32)
  address: string;
}
