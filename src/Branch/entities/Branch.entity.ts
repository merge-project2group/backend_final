import { Checkwork } from 'src/checkwork/entities/checkwork.entity';
import { Ingredient } from 'src/ingredients/entities/ingredient.entity';
import { User } from 'src/users/entities/user.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Branch {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  tel: string;

  @Column()
  address: string;

  @OneToMany(() => User, (Users) => Users.branch)
  user: User[];

  @OneToMany(() => Ingredient, (ingredients) => ingredients.branch)
  ingredients: Ingredient[];

  @OneToMany(() => Checkwork, (checkworks) => checkworks.branch)
  checkwork: Checkwork;
}
