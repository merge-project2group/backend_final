import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  UseGuards,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';

import { AuthGuard } from 'src/auth/auth.guard';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { uuid } from 'uuidv4';
import { extname } from 'path';
import { OrderIngredientsService } from './orderingredients.service';
import { UpdateOrderIngredientDto } from './dto/update-OrderIngredient.dto';
import { CreateOrderIngredientDto } from './dto/create-OrderIngredient.dto';

//@UseGuards(AuthGuard)
@Controller('orderingredients')
export class OrderIngredientsController {
  constructor(
    private readonly orderingredientsService: OrderIngredientsService,
  ) {}

  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/orderingredients',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  create(
    @Body() createIngredientDto: CreateOrderIngredientDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      createIngredientDto.image = file.filename;
    }
    return this.orderingredientsService.create(createIngredientDto);
  }

  @Get()
  findAll() {
    return this.orderingredientsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.orderingredientsService.findOne(+id);
  }

  @Post(':id')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/orderingredients',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  update(
    @Param('id') id: string,
    @Body() updateIngredientDto: UpdateOrderIngredientDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      updateIngredientDto.image = file.filename;
    }
    return this.orderingredientsService.update(+id, updateIngredientDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.orderingredientsService.remove(+id);
  }
}
