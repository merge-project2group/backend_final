import { Test, TestingModule } from '@nestjs/testing';
import { OrderIngredientsController } from './orderingredients.controller';
import { OrderIngredientsService } from './orderingredients.service';

describe('IngredientsController', () => {
  let controller: OrderIngredientsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OrderIngredientsController],
      providers: [OrderIngredientsService],
    }).compile();

    controller = module.get<OrderIngredientsController>(
      OrderIngredientsController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
