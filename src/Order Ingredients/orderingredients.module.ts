import { Module } from '@nestjs/common';
import { OrderIngredientsService } from './orderingredients.service';
import { OrderIngredientsController } from './orderingredients.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderIngredient } from './entities/OrderIngredient.entity';
import { Ingredient } from 'src/ingredients/entities/ingredient.entity';

@Module({
  imports: [TypeOrmModule.forFeature([OrderIngredient, Ingredient])],
  controllers: [OrderIngredientsController],
  providers: [OrderIngredientsService],
})
export class OrderIngredientsModule {}
