export class CreateOrderIngredientDto {
  id: number;
  name: string;
  price: string;
  amount: number;
  Savedate: string;
  image: string;
  branch: number;
}
