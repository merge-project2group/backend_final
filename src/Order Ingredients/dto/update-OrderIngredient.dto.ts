import { PartialType } from '@nestjs/mapped-types';
import { CreateOrderIngredientDto } from './create-OrderIngredient.dto';

export class UpdateOrderIngredientDto extends PartialType(
  CreateOrderIngredientDto,
) {}
