import { Branch } from 'src/Branch/entities/Branch.entity';
import { Ingredient } from 'src/ingredients/entities/ingredient.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';

const formatDate = (date: Date): string => {
  const day = String(date.getDate()).padStart(2, '0');
  const month = String(date.getMonth() + 1).padStart(2, '0');
  const year = date.getFullYear();

  return `${day}/${month}/${year}`;
};

@Entity()
export class OrderIngredient {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  amount: number;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @Column({ default: formatDate(new Date()) })
  Savedate: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Branch, (branch) => branch.id)
  branch: Branch;

  @OneToMany(() => Ingredient, (ingredients) => ingredients.orderIngredient)
  ingredients: Ingredient[];
}
