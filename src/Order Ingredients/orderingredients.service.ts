import { Injectable } from '@nestjs/common';
import { CreateOrderIngredientDto } from './dto/create-OrderIngredient.dto';
import { UpdateOrderIngredientDto } from './dto/update-OrderIngredient.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { OrderIngredient } from './entities/OrderIngredient.entity';
import { Repository } from 'typeorm';
import { Ingredient } from 'src/ingredients/entities/ingredient.entity';

@Injectable()
export class OrderIngredientsService {
  constructor(
    @InjectRepository(OrderIngredient)
    private orderingredientsRepository: Repository<OrderIngredient>,
    @InjectRepository(Ingredient)
    private ingredientsRepository: Repository<Ingredient>,
  ) {}

  async create(createOrderIngredientDto: CreateOrderIngredientDto) {
    const orderingredient = new OrderIngredient();
    orderingredient.id = createOrderIngredientDto.id;
    orderingredient.name = createOrderIngredientDto.name;
    orderingredient.price = parseFloat(createOrderIngredientDto.price);
    orderingredient.amount = createOrderIngredientDto.amount;

    const fdate = new Date();
    const day = String(fdate.getDate()).padStart(2, '0');
    const month = String(fdate.getMonth() + 1).padStart(2, '0');
    const year = fdate.getFullYear();
    const currentDate = `${year}-${month}-${day}`;

    const ingredient = await this.ingredientsRepository.findOne({
      where: { name: orderingredient.name, Savedate: currentDate },
    });

    if (ingredient) {
      let x: number = ingredient.amount;
      let y: number = orderingredient.amount;
      let ans = 0;

      for (let i = 1; i < 300; i++) {
        if (ingredient.amount == i) {
          x = i;
        }
        if (orderingredient.amount == i) {
          y = i;
        }
      }
      ans = x + y;
      ingredient.amount = ans;
      await this.ingredientsRepository.save(ingredient);
    } else {
      const ningredient = new Ingredient();
      ningredient.name = createOrderIngredientDto.name;
      ningredient.price = parseFloat(createOrderIngredientDto.price);
      ningredient.amount = createOrderIngredientDto.amount;
      ningredient.Savedate = createOrderIngredientDto.Savedate;
      ningredient.used = 0;
      await this.ingredientsRepository.save(ningredient);
      return await this.orderingredientsRepository.save(ningredient);
    }
    // inge.amount += orderingredient.amount;
    // console.log(inge.amount);
    // console.log(orderingredient.Savedate);
    // console.log(inge);

    return await this.orderingredientsRepository.save(orderingredient);
  }

  findAll() {
    return this.orderingredientsRepository.find();
  }

  findOne(id: number) {
    return this.orderingredientsRepository.findOne({
      where: { id },
    });
  }

  async update(id: number, updateOrderIngredientDto: UpdateOrderIngredientDto) {
    const orderingredient = await this.orderingredientsRepository.findOneOrFail(
      {
        where: { id },
      },
    );
    orderingredient.name = updateOrderIngredientDto.name;
    orderingredient.price = parseFloat(updateOrderIngredientDto.price);
    orderingredient.amount = updateOrderIngredientDto.amount;

    this.orderingredientsRepository.save(orderingredient);
    const result = await this.orderingredientsRepository.findOne({
      where: { id },
    });
    return result;
  }

  async remove(id: number) {
    const deleteIngredient =
      await this.orderingredientsRepository.findOneOrFail({
        where: { id },
      });
    await this.orderingredientsRepository.remove(deleteIngredient);

    return deleteIngredient;
  }
}
