import { Injectable } from '@nestjs/common';
import { CreateMemberDto } from './dto/create-member.dto';
import { UpdateMemberDto } from './dto/update-member.dto';
import { DataSource, Repository } from 'typeorm';
import { Member } from './entities/member.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class MembersService {
  constructor(
    private dataSource: DataSource,
    @InjectRepository(Member) private membersRepository: Repository<Member>,
  ) { }
  create(createMemberDto: CreateMemberDto) {
    const member = new Member();
    member.name = createMemberDto.name;
    member.tel = createMemberDto.tel;
    return this.membersRepository.save(member);
  }

  findAll() {
    return this.membersRepository.find({});
  }

  findOne(id: number) {
    return this.membersRepository.findOne({
      where: { id },
    });
  }

  findTel(tel: string) {
    return this.membersRepository.findOne({
      where: { tel },
    });
  }

  async update(id: number, updateMemberDto: UpdateMemberDto) {
    const memberToUpdate = await this.membersRepository.findOneOrFail({
      where: { id },
    });
    memberToUpdate.name = updateMemberDto.name;
    memberToUpdate.tel = updateMemberDto.tel;
    return this.membersRepository.save(memberToUpdate);
  }

  async remove(id: number) {
    const deleteProduct = await this.membersRepository.findOneOrFail({
      where: { id },
    });
    await this.membersRepository.remove(deleteProduct);
    return deleteProduct;
  }

  async getNameByTel(id: string) {
    console.log(id)
    const result = await this.dataSource.query(
      'CALL getNameByTel(?);',
      [id],
    );
    return result[0];
  }
}
