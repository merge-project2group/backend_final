import { IsNotEmpty, Length } from 'class-validator';
export class CreateSalaryDto {
  date: string;

  userid: number;

  fullname: string;

  workinghour: number;

  workrate: number;

  typepay: string;

  status: string;

  salary: number;

  isChecked?: boolean;
}
