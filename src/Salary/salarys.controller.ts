/* eslint-disable prettier/prettier */
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { SalarysService } from './salarys.service';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';

@Controller('salarys')
export class SalarysController {
  constructor(private readonly salarysService: SalarysService) {}
  // Create
  @Post()
  create(@Body() createSalaryDto: CreateSalaryDto) {
    return this.salarysService.create(createSalaryDto);
  }

  // Read All
  @Get()
  findAll() {
    return this.salarysService.findAll();
  }

  @Get('/findExpensesSalaryPerBranch')
  findExpensesSalaryPerBranch() {
    return this.salarysService.findExpensesSalaryPerBranch();
  }

  // Read One
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.salarysService.findOne(+id);
  }

  @Get('/findPieYear/:id')
  findPieYear(@Param('id') id: string) {
    return this.salarysService.findPieYear(+id);
  }

  // Partial Update
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateBillDto: UpdateSalaryDto) {
    return this.salarysService.update(+id, updateBillDto);
  }
  // Delete
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.salarysService.remove(+id);
  }
}
