import { User } from 'src/users/entities/user.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Salary {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  date: string;

  @Column()
  userid: number;

  @Column()
  fullname: string;

  @Column()
  workinghour: number;

  @Column()
  workrate: number;

  @Column()
  typepay: string;

  @Column()
  status: string;

  @Column()
  salary: number;

  @Column()
  isChecked?: boolean;

}
