/* eslint-disable prettier/prettier */
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import { Salary } from './entities/salary.entity';

@Injectable()
export class SalarysService {
  constructor(
    @InjectRepository(Salary)
    private salarysRepository: Repository<Salary>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  create(createSalaryDto: CreateSalaryDto): Promise<Salary> {
    return this.salarysRepository.save(createSalaryDto);
  }

  // async create(createBillDto: CreateSalaryDto): Promise<Salary> {
  //   const user = await this.usersRepository.findOne({
  //     where: { id: createBillDto.userid },
  //   });
  //   if (!user) {
  //     throw new NotFoundException();
  //   }
  //   const newsalary = new Salary();
  //   newsalary.User = user;
  //   newsalary.fullname = user.fullName;
  //   return this.salarysRepository.save(newsalary);
  // }

  findAll(): Promise<Salary[]> {
    return this.salarysRepository.find();
  }

  findOne(id: number) {
    return this.salarysRepository.findOneBy({ id });
  }

  async update(id: number, updateBillDto: UpdateSalaryDto) {
    await this.salarysRepository.update(id, updateBillDto);
    const bill = await this.salarysRepository.findOneBy({ id });
    return bill;
  }

  async remove(id: number) {
    const deleteBill = await this.salarysRepository.findOneBy({ id });
    return this.salarysRepository.remove(deleteBill);
  }

  async findExpensesSalaryPerBranch() {
    const result = await this.salarysRepository.query('CALL `ExpensesSalaryPerBranch`();');
    return result[0];
  }

  async findPieYear(id: number) {
    const result = await this.salarysRepository.query(
      'CALL PieYear(?);',
      [id],
    );
    return result[0];
  }


}
