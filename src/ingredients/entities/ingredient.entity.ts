import { Branch } from 'src/Branch/entities/Branch.entity';
import { OrderIngredient } from 'src/Order Ingredients/entities/OrderIngredient.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Ingredient {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  amount: number;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @Column()
  Savedate: string;

  @Column({ default: 0 })
  used: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Branch, (branch) => branch.id)
  branch: Branch;

  @ManyToOne(() => OrderIngredient, (orderingrediernt) => orderingrediernt.id)
  orderIngredient: OrderIngredient;
}
