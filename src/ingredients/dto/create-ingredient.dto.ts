export class CreateIngredientDto {
  name: string;
  price: string;
  amount: number;
  image: string;
  Savedate: string;
  used: number;
  branch: number;
  orderIngredient: number;
}
