import { Branch } from 'src/Branch/entities/Branch.entity';
import { Salary } from 'src/Salary/entities/salary.entity';
import { Checkwork } from 'src/checkwork/entities/checkwork.entity';
import { Order } from 'src/orders/entities/order.entity';
import { Role } from 'src/roles/entities/role.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
  OneToMany,
  ManyToOne,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column({
    name: 'full_name',
    default: '',
  })
  fullName: string;

  @Column()
  gender: string;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @Column({ default: 'xxxxxxxxxx' })
  tel: string;

  @Column({ default: 0 })
  salary: number;

  @Column({ default: 'Staff' })
  position: string;

  @ManyToOne(() => Branch, (Branchs) => Branchs.user, { cascade: true })
  branch: Branch;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToMany(() => Role, (role) => role.users, { cascade: true })
  @JoinTable()
  roles: Role[];

  @OneToMany(() => Order, (order) => order.user)
  orders: Order[];

  @OneToMany(() => Checkwork, (checkworks) => checkworks.User)
  checkwork: Checkwork;
}
