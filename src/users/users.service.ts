/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from 'src/roles/entities/role.entity';
import { Branch } from 'src/Branch/entities/Branch.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Branch) private branchRepository: Repository<Branch>,
    @InjectRepository(Role) private rolesRepository: Repository<Role>,
  ) {}
  async create(createUserDto: CreateUserDto) {
    const user = new User();
    user.email = createUserDto.email;
    user.fullName = createUserDto.fullName;
    user.gender = createUserDto.gender;
    user.password = createUserDto.password;
    user.roles = JSON.parse(createUserDto.roles);
    user.tel = createUserDto.tel;
    user.salary = createUserDto.salary;
    user.position = createUserDto.position;
    const branch = await this.branchRepository.findOneBy({
      id: createUserDto.branch,
    });
    user.branch = branch;
    if (createUserDto.image && createUserDto.image !== '') {
      user.image = createUserDto.image;
    }

    return this.usersRepository.save(user);
  }

  findAll() {
    // eslint-disable-next-line prettier/prettier
    return this.usersRepository.find({
      relations: { roles: true, branch: true },
    });
  }

  findOne(id: number) {
    return this.usersRepository.findOne({
      where: { id },
      relations: { roles: true },
    });
  }

  findOneByEmail(email: string) {
    return this.usersRepository.findOneOrFail({
      where: { email },
      relations: ['branch'],
    });
  }

  finduserbybranch(branchid: number) {
    return this.usersRepository.find({
      // eslint-disable-next-line prettier/prettier
      where: { branch: { id: branchid } },
    });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = new User();
    user.email = updateUserDto.email;
    user.fullName = updateUserDto.fullName;
    user.gender = updateUserDto.gender;
    user.password = updateUserDto.password;
    user.roles = JSON.parse(updateUserDto.roles);
    user.tel = updateUserDto.tel;
    user.salary = updateUserDto.salary;
    user.position = updateUserDto.position;
    //user.branch = updateUserDto.branch;
    if (updateUserDto.image && updateUserDto.image !== '') {
      user.image = updateUserDto.image;
    }
    const updateUser = await this.usersRepository.findOneOrFail({
      where: { id },
      relations: { roles: true },
    });
    updateUser.email = user.email;
    updateUser.fullName = user.fullName;
    updateUser.gender = user.gender;
    updateUser.password = user.password;
    updateUser.image = user.image;
    updateUser.tel = user.tel;
    updateUser.salary = user.salary;
    updateUser.position = user.position;
    updateUser.branch = user.branch;
    for (const r of user.roles) {
      //เพิ่มใหม่
      const searchRole = updateUser.roles.find((role) => role.id === r.id);
      if (!searchRole) {
        const newRole = await this.rolesRepository.findOneBy({ id: r.id });
        updateUser.roles.push(newRole);
      }
    }
    for (let i = 0; i < updateUser.roles.length; i++) {
      //ลบออก
      const index = user.roles.findIndex(
        (role) => role.id === updateUser.roles[i].id,
      );
      if (index < 0) {
        updateUser.roles.splice(i, 1);
      }
    }
    await this.usersRepository.save(updateUser);
    const result = await this.usersRepository.findOne({
      where: { id },
      relations: { roles: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteProduct = await this.usersRepository.findOneOrFail({
      where: { id },
    });
    await this.usersRepository.remove(deleteProduct);

    return deleteProduct;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async findlowSalary() {
    const result = await this.usersRepository.query('CALL `getLowSalary`();');
    return result[0];
  }

  async getUseridbybranchid(id: number) {
    const result = await this.usersRepository.query(
      'CALL getUserIdbyBranchId(?);',
      [id],
    );
    return result[0];
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async findRateSalaryUser(min: number, max: number, idbranch: number) {
    const result = await this.usersRepository.query(
      'CALL getRateSalaryUser(?, ?, ?);',
      [min, max, idbranch],
    );
    return result;
  }

  async getCountEntriesByYear(id: number) {
    const result = await this.usersRepository.query(
      'CALL CountEntriesByYear(?);',
      [id],
    );
    return result[0];
  }

  async getCountNameByBranchId(id: number) {
    const result = await this.usersRepository.query(
      'CALL CountNameByBranchId(?);',
      [id],
    );
    return result[0];
  }
}
