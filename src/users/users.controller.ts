/* eslint-disable prettier/prettier */
import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  UseGuards,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { AuthGuard } from 'src/auth/auth.guard';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { uuid } from 'uuidv4';
import { extname } from 'path';

// @UseGuards(AuthGuard)
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/users',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  create(
    @Body() createUserDto: CreateUserDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      createUserDto.image = file.filename;
    }
    return this.usersService.create(createUserDto);
  }

  @Get()
  findAll() {
    return this.usersService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.usersService.findOne(+id);
  }

  @Get('/Useridbybranchid/:id')
  getUserbybranchid(@Param('id') id: number) {
    return this.usersService.getUseridbybranchid(id);
  }

  @Get('/users/fRateSalaryUser/:min/:max/:idbranch')
  findRateSalaryUser(
    @Param('min') min: number,
    @Param('max') max: number,
    @Param('idbranch') idbranch: number,
  ) {
    return this.usersService.findRateSalaryUser(min, max, idbranch);
  }

  @Get('/users/findlow')
  findLowsalay() {
    return this.usersService.findlowSalary();
  }

  @Get('/branch/:id')
  finduserbybranch(@Param('id') id: string) {
    return this.usersService.finduserbybranch(+id);
  }

  @Get('/getCountNameByBranchId/:id')
  getCountNameByBranchId(@Param('id') id: string) {
    return this.usersService.getCountNameByBranchId(+id);
  }

  @Post(':id')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/users',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  update(
    @Param('id') id: string,
    @Body() updateUserDto: UpdateUserDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      updateUserDto.image = file.filename;
    }
    return this.usersService.update(+id, updateUserDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usersService.remove(+id);
  }
  @Post('upload')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/users',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  uploadFile(
    @Body() user: { name: string; age: number },
    @UploadedFile() file: Express.Multer.File,
  ) {
    console.log(user);
    console.log(file.filename);
    console.log(file.path);
  }

  @Get('/CountEntriesByYear/:id')
  getCountEntriesByYear(@Param('id') id: string) {
    return this.usersService.getCountEntriesByYear(+id);
  }
}
